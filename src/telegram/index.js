require('dotenv').config();
const Telegram = require ('node-telegram-bot-api');
const bot = new Telegram(process.env.TOKEN_TELEGRAM_API, { polling: true });
//const fech = require ('node-fech');
const {Alquiler} = require('../models/index')
const fs = require ('fs');

bot.onText(/^\/start/, function(msg){
    console.log(msg);
    var chatId = msg.chat.id;
    var firstnameuser = msg.from.first_name;
    const alquiler = {
        idusuario: msg.chat.first_name
    };
    Alquiler.create([alquiler]);
    
    bot.sendMessage(chatId, `Hola, ${firstnameuser} soy el bot de RentCars \n`+
                            "¿En qué te puedo ayudar ? \n"+
                            "Escribe el número de la opción a realizar \n"+
                            "1. Reservar vehiculo 🚗 \n"+
                            "2. Vehiculos disponibles 🔎 \n"+
                            "3. Más información 🧐");
});

bot.on('message', function(msg){
    var saludo = "hola";
    var firstnameuser = msg.from.first_name;
    var chatId = msg.chat.id;
    alquiler = {
        idusuario: msg.chat.first_name
    };
    Alquiler.create([alquiler]);
    if(msg.text.toString().toLowerCase().includes(saludo)){
        bot.sendMessage(chatId, `Hola, ${firstnameuser} soy el bot de RentCars \n`+
        "¿En qué te puedo ayudar ? \n"+
        "Escriba el número de la opción a realizar \n"+
        "1. Reservar vehiculo 🚗 \n"+
        "2. Vehiculos en alquiler 🔎 \n"+
        "3. Más información 🧐");}
});

bot.on('message', function(msg){
    var opcion1 = "menú";
    var opcion2 = "menu";
    var chatId = msg.chat.id;
    if(msg.text.toString().toLowerCase().includes(opcion1) || msg.text.toString().toLowerCase().includes(opcion2)){
        bot.sendMessage(chatId,
        "Escriba el número de la opción a realizar \n"+
        "1. Reservar vehiculo 🚗 \n"+
        "2. Vehiculos en alquiler 🔎 \n"+
        "3. Más información 🧐");}
});

bot.on('message', function(msg){
    var opcion = "1";
    var palabra = "Reservar vehiculo";
    var chatId = msg.chat.id;
    if(msg.text.toString().toLowerCase().includes(opcion) || msg.text.toString().toLowerCase().includes(palabra)){
    bot.sendMessage(chatId, `Escriba el número o la palabra de las opciones disponibles \n`+
    "1. Solicitar vehiculo🔎 \n"+
    "2. Vehiculos disponibles🚗");
    } 
});

bot.on('message', function(msg){
    var opcion = "2";
    var palabra = "Reservar vehiculo";
    var chatId = msg.chat.id;
    if(msg.text.toString().toLowerCase().includes(opcion) || msg.text.toString().toLowerCase().includes(palabra)){
        bot.sendMessage(chatId, `Escriba el número o la palabra de las opciones disponibles \n`+
        "1. Familiar conchevino \n"+
        "2. Familiar gris \n"+
        "3. Furgoneta \n"+
        "4. Limocina \n"+
        "5. Chevrolet sail");
        
    } 
});

bot.on('message', function(msg){

    let img_vehiculo = msg.text.toLowerCase();
    const exists = fs.existsSync(`./src/public/${img_vehiculo}.jpg`);

    if(exists){
        bot.sendPhoto(msg.from.id, `./src/public/${img_vehiculo}.jpg`, {caption:  `El vehiculo: ${img_vehiculo}`});
    }else{
        bot.sendMessage(msg.from.id, `El vehiculo ${img_vehiculo}.jpg no existe`);
    }
    
});

bot.on('message', function(msg){
    var opcion = "3";
    var palabra = "Reservar vehiculo";
    var firstnameuser = msg.from.first_name;
    var chatId = msg.chat.id;
    if(msg.text.toString().toLowerCase().includes(opcion) || msg.text.toString().toLowerCase().includes(palabra)){
        bot.sendMessage(chatId, `Escriba EDAD o ASPECTOS para mas imformación \n`+
        "1. ¿Que edad debo tener para alquilar un coche?"+
        "2. ¿Que aspectos son los más importantes a la hora de elgir un coche?");
    } 
});

bot.on('message', function(msg){
    var palabra = "edad";
    var chatId = msg.chat.id;
    if(msg.text.toString().toLowerCase().includes(palabra)){
        bot.sendMessage(chatId, `Edad`);
        bot.sendMessage(chatId, `La mayoría de nuestros proveedores exigen que el conductor principal tenga una edad de entre 21 y 70 años. Si el conductor es menor de 25 años o mayor de 70, es posible que tenga que abonar una tasa adicional.`);
    } 
});



bot.on('message', function(msg){
    var palabra = "aspectos";
    var chatId = msg.chat.id;
    if(msg.text.toString().toLowerCase().includes(palabra)){
        bot.sendMessage(chatId, `Tamaño: viajará con más comodidad si escoge un vehículo que disponga de suficiente espacio para los pasajeros y las maletas.`);
        bot.sendMessage(chatId, `Política de combustible: ¿No piensas conducir mucho? Con la política de combustible Depósito al mismo nivel, puedes ahorrar mucho dinero.`);
        bot.sendMessage(chatId, `Ubicación: le resultará más cómodo escoger un proveedor cuya oficina de alquiler se encuentre dentro del mismo aeropuerto; sin embargo, los proveedores situados fuera del aeropuerto ofrecen un autobús de cortesía que le llevará a la oficina y suelen tener precios más económicos.`);
    } 
});

module.exports = { bot };