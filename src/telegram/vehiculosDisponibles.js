require('dotenv').config();
const Telegram = require ('node-telegram-bot-api');
const bot = new Telegram(process.env.TOKEN_TELEGRAM_API, { polling: true });
//const fech = require ('node-fech');
const fs = require ('fs');

bot.on('message', function(msg){

    let img_vehiculo = msg.text.toLowerCase();
    const exists = fs.existsSync(`./src/public/${img_vehiculo}.jpg`);

    if(exists){
        bot.sendPhoto(msg.from.id, `./src/public/${img_vehiculo}.jpg`, {caption:  `El vehiculo: ${img_vehiculo}`});
    }else{
        bot.sendMessage(msg.from.id, `El vehiculo ${img_vehiculo}.jpg no existe`);
    }
    
});

module.exports = VehiculoDisponibles;