const mongoose = require('mongoose');

const {Schema}=mongoose

const vehiculoSchema = new Schema(
    {
        idusuario:{type:String},
        nomApe: {type:String},
        email:{type:String},
        ci:{type:String},        
        domicilio:{type:String},
        celular:{type:String},
        tipovehiculo:{type:String},        
        tiempo:{type:String},
        tipolicencia:{type:String}
    },
    {
        timestamps:true
    }
)

const Vehiculo = mongoose.model("Vehiculo", vehiculoSchema)
module.exports = Vehiculo;