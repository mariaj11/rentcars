const mongoose = require('mongoose');

const {Schema}=mongoose

const alquilerSchema = new Schema(
    {
        idusuario:{type:String},
        nomApe: {type:String},
        email:{type:String},
        ci:{type:String},        
        domicilio:{type:String},
        celular:{type:String},
        tipovehiculo:{type:String},        
        tiempo:{type:String},
        tipolicencia:{type:String}
    },
    {
        timestamps:true
    }
)

const Alquiler = mongoose.model("Alquiler", alquilerSchema)
module.exports = Alquiler;